import bb.cascades 1.2

Page {
    onCreationCompleted: {
        _app.init(container);
    }
    
    Container {
        id: container
        topPadding: 20.0
    }
}