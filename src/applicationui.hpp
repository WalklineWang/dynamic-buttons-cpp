#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include <bb/cascades/Button>
#include <bb/cascades/Container>

namespace bb {
    namespace cascades {
        class Application;
        class LocaleHandler;
        class Container;
    }
}

class QTranslator;

class ApplicationUI : public QObject
{
    Q_OBJECT

public:
    ApplicationUI(bb::cascades::Application *app);
    virtual ~ApplicationUI() { }

    Q_INVOKABLE void init(QObject *container);

public slots:
	void handleClicked();

private slots:
    void onSystemLanguageChanged();

private:
    QTranslator* m_pTranslator;
    bb::cascades::LocaleHandler* m_pLocaleHandler;
    bb::cascades::Container* m_Container;

    bb::cascades::Button m_Buttons[20];
    QString m_List[10];
};

#endif
