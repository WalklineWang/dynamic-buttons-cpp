#include <bb/cascades/Application>

#include <QLocale>
#include <QTranslator>
#include "applicationui.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <Qt/qdeclarativedebug.h>
#include <bb/system/SystemToast>

using namespace bb::cascades;

void myMessageOutput(QtMsgType type, const char *msg)
 {
     switch (type) {
     case QtDebugMsg:
         fprintf(stderr, "Debug: %s\n", msg);
         break;
     case QtWarningMsg:
         fprintf(stderr, "Warning: %s\n", msg);
         break;
     case QtCriticalMsg:
         fprintf(stderr, "Critical: %s\n", msg);
         break;
     case QtFatalMsg:
         fprintf(stderr, "Fatal: %s\n", msg);
         abort();
     }
 }

Q_DECL_EXPORT int main(int argc, char **argv)
{
	qInstallMsgHandler(myMessageOutput);

	qmlRegisterType<bb::system::SystemToast>("bb.system", 1, 0, "SystemToast");

    Application app(argc, argv);

    new ApplicationUI(&app);

    return Application::exec();
}
