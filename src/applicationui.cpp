#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/Container>
#include <bb/system/SystemToast>

using namespace bb::cascades;
using namespace bb::system;

void ApplicationUI::handleClicked()
{
	Button *button=qobject_cast<Button *> (sender());

	for(int i=0; i<m_List->size(); i++)
	{
		QString objName=m_List[i];
		if(button->objectName()==objName)
		{
			SystemToast tips;
			tips.setBody("\"" + objName + "\" clicked");
			tips.setAutoUpdateEnabled(true);
			tips.exec();
			qDebug() << objName << "clicked";
			break;
		}
	}
}

void ApplicationUI::init(QObject* container)
{
    m_Container=dynamic_cast<Container *> (container);
}

ApplicationUI::ApplicationUI(bb::cascades::Application *app) : QObject(app)
{
    m_pTranslator = new QTranslator(this);
    m_pLocaleHandler = new LocaleHandler(this);

    bool res = QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()));
    Q_ASSERT(res);
    Q_UNUSED(res);

    onSystemLanguageChanged();

    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);
    qml->setContextProperty("_app", this);
    AbstractPane *root = qml->createRootObject<AbstractPane>();
    app->setScene(root);

    for(int i=0; i<10; i++)
    {
    	m_List[i]="This is Button " + QString::number(i, 10);

    	m_Buttons[i].create();
    	m_Buttons[i].setText(m_List[i]);
    	m_Buttons[i].setHorizontalAlignment(HorizontalAlignment::Center);
    	m_Buttons[i].setObjectName(m_List[i]);

    	bool connectResult;
    	Q_UNUSED(connectResult);
    	connectResult=QObject::connect(&m_Buttons[i], SIGNAL(clicked()), this, SLOT(handleClicked()));
    	Q_ASSERT(connectResult);

    	m_Container->add(&m_Buttons[i]);
    }
}

void ApplicationUI::onSystemLanguageChanged()
{
    QCoreApplication::instance()->removeTranslator(m_pTranslator);
    QString locale_string = QLocale().name();
    QString file_name = QString("DynamicButtons_%1").arg(locale_string);
    if (m_pTranslator->load(file_name, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(m_pTranslator);
    }
}
