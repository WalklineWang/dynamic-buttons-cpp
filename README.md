A sample demonstrated how to add buttons by dynamic way and handle their click event（C++）
----------------------------------------------

### Preview ###
![preview](http://img.blog.csdn.net/20131220145843593 "preview")

## How to import this project ##

1.  In the Momentics IDE, on the **File** menu, click **Import**.
*  Expand **General** and select **Existing Projects into Workspace**. Click **Next**.
*  Select the **Select root directory** option and click **Browse**.
*  Browse to the location of the project that you want to import, and click **OK**.
*  In the **Projects** section, select the project that you want to import, and click **Finish**. You'll see the imported project in the Project Explorer view, and you can explore the source code.

## Reference ##
*  [BB10: Handling Several Buttons Click Event in CPP](http://supportforums.blackberry.com/t5/Native-Development/BB10-Handling-Several-Buttons-Click-Event-in-CPP/m-p/2134563/highlight/true#M13273)<br />
*  [QObject * QObject::sender () const](https://developer.blackberry.com/native/reference/cascades/qobject.html#sender)<br />

		qDebug() << "Written by Walkline Wang."